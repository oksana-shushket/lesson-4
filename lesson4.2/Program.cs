﻿using System.Linq;
using System;
using System.Collections.Specialized;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Enter the number of elements in array");

        int index = Convert.ToInt32(Console.ReadLine());
        int[] array = new int[index];
        int summ = 0;
        Random random = new Random();

        for (int i = 0; i < array.Length; i++)
        {
            array[i] = random.Next(-1000,1000);
        }

        for (int i = 0; i < array.Length; i++)
        {
            summ += array[i];
        }

        int midNum = summ / array.Length;

        Console.WriteLine("[{0}]", string.Join(", ", array));
        Console.WriteLine("Minimum number is " + array.Min());
        Console.WriteLine("Maximum number is " + array.Max());
        Console.WriteLine("Arithmetic mean is " + midNum);
    }
}






