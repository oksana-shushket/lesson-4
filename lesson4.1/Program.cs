﻿using System;
using System.Linq;

internal class Program
{
    private static void Main(string[] args)
    {
            int[] mass = { 0, 1, 10, 23, 67, 34, };
            var mass1 = string.Join(",", mass);
            Console.WriteLine(mass1);

            Console.WriteLine("Enter a number you wan to delete");
            var numb1 = Convert.ToInt32(Console.ReadLine());

            bool exist = mass.Contains(numb1);
            if (exist)
            {
                Console.WriteLine("Element found and deleted");
                mass = mass.Where(e => e != numb1).ToArray();
                Console.WriteLine(string.Join(",", mass));
            }
            else
            {
                Console.WriteLine("Element not found");
            }
    }
}