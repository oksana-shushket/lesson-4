﻿using System;
internal class Program
{
    private static void Main(string[] args)
    {  
        Random random = new Random();
        int summury1 = 0;
        int[] array1 = new int[5];
        int summury2 = 0;
        int[] array2 = new int[5];

        for (int i = 0; i < array1.Length; i++)
        {
            array1[i] = random.Next(-100, 100);
            summury1 += array1[i];
            array2[i] = random.Next(-100, 100);
            summury2 += array2[i];
        }

        int middleOfArray1 = summury1 / array1.Length;
        int middleOfArray = summury2 / array2.Length;

        Console.WriteLine("Your first array is " + string.Join(" ", array1));
        Console.WriteLine("The mid number in your first array is " + middleOfArray1);
        Console.WriteLine("Your second array is " + string.Join(" ", array2));
        Console.WriteLine("The mid number in your second array is " + middleOfArray);
       
        if (middleOfArray1 > middleOfArray)
        {
            Console.WriteLine($"The mid number in first array is bigger ");
        }

        else if (middleOfArray1 < middleOfArray)
        {
            Console.WriteLine($"The mid number in second array is bigger ");
        }

        else if (middleOfArray1 == middleOfArray)
        {
            Console.WriteLine($"The mid numbers are equal ");
        }
    }
}